##

This project is a remake of the classic Pong game, made within the Unity3D engine, with some big changes to the game's feel, goal, mechanics...

## Installation

To play the game, check out the tag page [here](https://gitlab.com/newproxima/pong/-/tags), select and download the most recent release adapted to your system. In the downloaded folder, launch the executable.

Alternativly, download the last commit on the main branch, open the project with Unity 3D and build the game if you know how to or play it in the editor !

Enjoy !
