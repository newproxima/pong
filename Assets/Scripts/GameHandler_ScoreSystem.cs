using UnityEngine;
using Action = System.Action;

public partial class GameHandler : MonoBehaviour
{
    public event Action ScoresChanged;
    public event Action RuleChanged;

    public enum Rules { Classic, Avoid, TopRight_ForLeft, BottomRight_ForLeft, BottomLeft_ForLeft, TopLeft_ForLeft, TopRight_ForRight, BottomRight_ForRight, BottomLeft_ForRight, TopLeft_ForRight} ;


    [Header("Rules")]
    [SerializeField] private Rules startingRule = Rules.Classic ;
    Rules currentRule;
    public int ruleChangeInterval = 5 ; // in seconds
    public float ruleChangeCountdown { get; private set; } // actually used to change rules

    public int score_leftPlayer { get; private set; }
    public int score_rightPlayer { get; private set; }

    [Header("Trigger zone references")]
    [SerializeField] Goalable leftPlayer ;
    [SerializeField] Goalable rightPlayer ;
    [SerializeField] Goalable leftWall ;
    [SerializeField] Goalable rightWall ;
    [SerializeField] Goalable topLeftWall ;
    [SerializeField] Goalable topRightWall ;
    [SerializeField] Goalable bottomLeftWall ;
    [SerializeField] Goalable bottomRightWall ;

    void InitScoreSystem()
    {
        Random.InitState(System.Guid.NewGuid().GetHashCode());

        score_leftPlayer = 0;
        score_rightPlayer = 0;
        
        ruleChangeCountdown = ruleChangeInterval ;
        
        SetRule(startingRule);
    }

    void ScoreSystemTick()
    {
        ruleChangeCountdown -= 1 * Time.deltaTime ;

        if (ruleChangeCountdown <= 0)
        {
            ChangeRule() ;

            ruleChangeCountdown = ruleChangeInterval ;
        }
    }

    void ChangeRule()
    {
        int rulesCount = System.Enum.GetValues(typeof(Rules)).Length;

        int rd;
        do
        {
            rd = Random.Range(0, rulesCount);
        } while((int)currentRule == rd);

        SetRule((Rules)rd);
    }

    void SetRule(Rules rule)
    {
        ResetSubscriptions();
        
        switch (rule)
        {
            // classic pong rule, players score when the ball hits the opposite wall
            case Rules.Classic:
                rightWall.Hit += LeftScores;
                leftWall.Hit += RightScores;

                rightWall.SetAsLeftPlayerTarget();
                leftWall.SetAsRightPlayerTarget();
                break;

            // players score when the ball hits the opposite player
            case Rules.Avoid:
                rightPlayer.Hit += LeftScores;
                leftPlayer.Hit += RightScores;

                rightPlayer.SetAsLeftPlayerTarget();
                leftPlayer.SetAsRightPlayerTarget();
                break ;

            // left player scores when the ball hits the top left wall
            case Rules.TopLeft_ForLeft:
                topLeftWall.Hit += LeftScores;
                topLeftWall.SetAsLeftPlayerTarget();
                break ;
            
            // left player scores when the ball hits the top right wall
            case Rules.TopRight_ForLeft: 
                topRightWall.Hit += LeftScores;
                topRightWall.SetAsLeftPlayerTarget();
                break ;

            // left player scores when the ball hits the bottom left wall
            case Rules.BottomLeft_ForLeft:
                bottomLeftWall.Hit += LeftScores;
                bottomLeftWall.SetAsLeftPlayerTarget();
                break ;

            // left player scores when the ball hits the bottom right wall
            case Rules.BottomRight_ForLeft: 
                bottomRightWall.Hit += LeftScores;
                bottomRightWall.SetAsLeftPlayerTarget();
                break ;

            // right player scores when the ball hits the top left wall
            case Rules.TopLeft_ForRight:
                topLeftWall.Hit += RightScores;
                topRightWall.SetAsRightPlayerTarget();
                break ;

            // right player scores when the ball hits the top right wall
            case Rules.TopRight_ForRight:
                topRightWall.Hit += RightScores;
                topRightWall.SetAsRightPlayerTarget();
                break ;

            // right player scores when the ball hits the bottom left wall
            case Rules.BottomLeft_ForRight:
                bottomLeftWall.Hit += RightScores;
                bottomLeftWall.SetAsRightPlayerTarget();
                break ;

            // right player scores when the ball hits the bottom right wall
            case Rules.BottomRight_ForRight:
                bottomRightWall.Hit += RightScores;
                bottomRightWall.SetAsRightPlayerTarget();
                break ;

            default:
                throw new System.Exception("No corresponding rule found !");
        }

        currentRule = rule ;
        RuleChanged?.Invoke();
        Debug.Log("NEW RULE : " + currentRule.ToString());
    }

    void ResetSubscriptions()
    {
        leftPlayer.Hit -= LeftScores;
        rightPlayer.Hit -= LeftScores;
        leftWall.Hit -= LeftScores;
        rightWall.Hit -= LeftScores;
        topLeftWall.Hit -= LeftScores;
        topRightWall.Hit -= LeftScores;
        bottomLeftWall.Hit -= LeftScores;
        bottomRightWall.Hit -= LeftScores;

        leftPlayer.Hit -= RightScores;
        rightPlayer.Hit -= RightScores;
        leftWall.Hit -= RightScores;
        rightWall.Hit -= RightScores;
        topLeftWall.Hit -= RightScores;
        topRightWall.Hit -= RightScores;
        bottomLeftWall.Hit -= RightScores;
        bottomRightWall.Hit -= RightScores;

        leftPlayer.SetAsNoTarget();
        rightPlayer.SetAsNoTarget();
        leftWall.SetAsNoTarget();
        rightWall.SetAsNoTarget();
        topLeftWall.SetAsNoTarget();
        topRightWall.SetAsNoTarget();
        bottomLeftWall.SetAsNoTarget();
        bottomRightWall.SetAsNoTarget();
    }

    void LeftScores(string from)
    {
        score_leftPlayer++;
        // Debug.Log("gained from " + from);
        ScoresChanged?.Invoke();
    }     
    void RightScores(string from)
    {
        score_rightPlayer++;
        // Debug.Log("gained from " + from);
        ScoresChanged?.Invoke();
    }
}
