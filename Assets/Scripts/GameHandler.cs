using UnityEngine;

public partial class GameHandler : MonoBehaviour
{
    public static GameHandler _instance;

    [SerializeField] Rigidbody2D rb_ball;
    [SerializeField] Animator _animator;

    [Header("Ball settings")]
    [SerializeField, Range(20f, 90f)] private float maxAngle = 70f;
    public float ballSlowTime { get; private set; }
    [SerializeField, Range(1f, 5f)] float speedBoostCooldown = 2f;
    [SerializeField, Range(1f, 30f)] float pushStrength = 20f;


    [Header("Colours")]
    public Color defaultColour = Color.white;
    public Color leftColour = Color.red;
    public Color rightColour = Color.green;
    [SerializeField] Color ballColour;
    [SerializeField] Color slowBallColour;


    public bool paused { get; private set; }
    bool anim_started = false;
    Vector3 capturedBallMotion;

    void Awake()
    {
        if(_instance != null) throw new System.Exception("There may only be one instance of GameHandler");
        _instance = this;
    
    }

    void Start() 
    {
        PushBall();
        Transition();

        InitScoreSystem();
    }

    void Update()
    {
        if(!paused)
        {
            // ensures the ball keeps a good horizontal velocity
            if(ballSlowTime >= speedBoostCooldown)
            {
                PushBall();
                Debug.Log("SPEEEEED");

                ballSlowTime = 0f;
            }
            else
            {
                // checks if the velocity vector has an angle within the maxAngle range
                float currentY = rb_ball.velocity.normalized.y;
                float maxAngleRad = Mathf.Deg2Rad * maxAngle;
                if(rb_ball.velocity.magnitude < pushStrength * 0.95f || (currentY > 0 && currentY > Mathf.Sin(maxAngleRad)) || (currentY < 0 && currentY < -Mathf.Sin(maxAngleRad)))
                {
                    ballSlowTime += Time.deltaTime;
                    rb_ball.GetComponent<SpriteRenderer>().color = slowBallColour;
                }
                else
                {
                    rb_ball.GetComponent<SpriteRenderer>().color = ballColour;
                    ballSlowTime = 0f;
                }
            }

            ScoreSystemTick();
        }

        if(anim_started)
            Transition();
    }

    public void Transition()
    {
        if(anim_started)
        {
            if(_animator.GetCurrentAnimatorStateInfo(0).IsName("Cage Entrance") && 
                _animator.GetCurrentAnimatorStateInfo(0).normalizedTime >= 1.0f)
            {
                anim_started = false;
                Resume();
            }
        }
        else
        {
            Pause();
            _animator.SetTrigger("Start");
            anim_started = true;
        }
    }

    public void Pause()
    {
        paused = true;
        capturedBallMotion = rb_ball.velocity;
        rb_ball.velocity = Vector3.zero;
    }

    public void Resume()
    {
        paused = false;
        rb_ball.velocity = capturedBallMotion;
    }

    void PushBall()
    {
        Vector2 a;
        do {
            a = UnityEngine.Random.insideUnitCircle.normalized;
        } while((a.y > 0 && a.y > a.x) || (a.y < 0 && a.y < a.x));
        // ensures the angle of the new vector is within ]-45°, 45°[ 
        // (45° is the tipping point where x and y are equal)

        rb_ball.velocity = a * pushStrength;
    }

    void OnDrawGizmos()
    {
        Vector3 pos = rb_ball.transform.position;
        Vector3 vel = rb_ball.velocity.normalized;
        Gizmos.color = Color.cyan;
        Gizmos.DrawLine(pos, pos + new Vector3(vel.x, vel.y, 0));

        float maxAngleRad = Mathf.Deg2Rad * maxAngle;

        Gizmos.color = Color.red;
        if(vel.x > 0)
        {
            Gizmos.DrawLine(pos, pos + new Vector3(Mathf.Cos(maxAngleRad), Mathf.Sin(maxAngleRad), 0));
            Gizmos.DrawLine(pos, pos + new Vector3(Mathf.Cos(maxAngleRad), -Mathf.Sin(maxAngleRad), 0));
        }
        else if (vel.x < 0)
        {
            Gizmos.DrawLine(pos, pos + new Vector3(-Mathf.Cos(maxAngleRad), Mathf.Sin(maxAngleRad), 0));
            Gizmos.DrawLine(pos, pos + new Vector3(-Mathf.Cos(maxAngleRad), -Mathf.Sin(maxAngleRad), 0));
        }
    }
}
