using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    enum Controls { ZS, UpDown }

    [SerializeField] Controls _controls;
    [SerializeField] float speed;
    [SerializeField] float maxDistance;

    void Update()
    {
        HandleInputs();
    }

    void HandleInputs()
    {
        string verticalAxis = "";
        switch (_controls)
        {
            case Controls.ZS:
                verticalAxis = "ZS Vertical";
                break;
            case Controls.UpDown:
                verticalAxis = "Arrows Vertical";
                break;
            default:
                throw new System.Exception("huh? (inputs)");
        }

        float verticalInput = Input.GetAxis(verticalAxis);

        Move(Vector3.up * verticalInput * speed * Time.deltaTime);
    }

    void Move(Vector3 by)
    {
        transform.position += by;

        if(transform.position.y > maxDistance)
        {
            transform.position = new Vector3(transform.position.x, maxDistance, transform.position.z);
        }
        if(transform.position.y < -maxDistance)
            transform.position = new Vector3(transform.position.x, -maxDistance, transform.position.z);
    }
}
