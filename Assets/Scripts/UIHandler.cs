using UnityEngine;
using UnityEngine.SceneManagement;
using TextMesh = TMPro.TextMeshProUGUI;

public class UIHandler : MonoBehaviour
{
    [SerializeField] TextMesh txt_score1;
    [SerializeField] TextMesh txt_score2;
    [SerializeField] TextMesh txt_ruleCooldown;

    GameHandler handler;

    void Start()
    {
        handler = GameHandler._instance;

        txt_score1.color = handler.leftColour;
        txt_score2.color = handler.rightColour;
        txt_ruleCooldown.color = handler.defaultColour;

        handler.ScoresChanged += UpdateScores;

        UpdateScores();
    }

    void Update()
    {
        txt_ruleCooldown.text = ((int)(handler.ruleChangeCountdown * 10) / 10f).ToString();
        
        if(Input.GetButtonDown("Pause"))
        {
            if(!handler.paused)
            {
                SceneManager.LoadScene(2, LoadSceneMode.Additive);
                handler.Pause();
            }
            else
            {
                SceneManager.UnloadSceneAsync(2);
                handler.Resume();
            }
        }
    }

    void UpdateScores()
    { 
        txt_score1.text = handler.score_leftPlayer.ToString();
        txt_score2.text = handler.score_rightPlayer.ToString();
    }
}
