using System;
using System.Collections.Generic;
using UnityEngine;

public class ConsoleGUI : MonoBehaviour
{
    [Header("Window properties")]
    public bool showConsole;

    public Vector2 consolePosition = new Vector2(10f, 10f);
    public Vector2 consoleSize = new Vector2(Screen.width / 2.0f, Screen.width / 3.0f);
    [Min(25.0f)] public float inputHeight = 30.0f;
    [Min(25.0f)] public float outputLinesHeight = 25f;

    [Header("Console styling")]
    public GUISkin consoleSkin;

    public Color logColour = Color.cyan;
    public Color errorColour = Color.red;
    public Color predictionColour = new Color(0.5f, 0.8f, 0.5f, 1f);
    

    private ConsoleController controller;
    private List<GUIMessage> outputMessages;
    private string lastPredicted;
    private string input;

    private int autocompleteIndex;

    private void Awake()
    {
        outputMessages = new List<GUIMessage>();

        GUIMessage.LogColour = logColour;
        GUIMessage.ErrorColour = errorColour;

        input = "";
        lastPredicted = "";
    }
    private void Start()
    {
        controller = ConsoleController._instance;
        controller.ClearConsole += Controller_OnClearConsole;
        controller.PrintMessage += Controller_OnPrintMessage;    
    }
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Dollar))
            ToggleConsole();
        

        if(lastPredicted != input)
        {
            controller.PredictCommand(input);
            lastPredicted = input;
            autocompleteIndex = 0;
        }
    }

    public void ToggleConsole()
    {
        showConsole = !showConsole;

        // invoke help command on clist to prompt the user with
        // information about this potentially usefull command
        // upon opening an empty console
        if(showConsole && outputMessages.Count == 0)
            ConsoleController.HELP.Invoke("clist");
    }
    public void ValidateInput()
    {
        controller.ValidateInput(input);
        input = "";
    }
    [Obsolete("Text fields' cursor position after completion must be fixed")] public void Autocomplete()
    {
        if(controller.predictions != null && controller.predictions.Count > 0)
        {
            if(autocompleteIndex >= controller.predictions.Count)
                autocompleteIndex = 0;
            
            input = controller.predictions[autocompleteIndex];
            // Debug.Log(Input.compositionCursorPos);

            autocompleteIndex++;
        }
    }

    private void Controller_OnClearConsole(object sender, EventArgs e)
    {
        outputMessages.Clear();
    }
    private void Controller_OnPrintMessage(object sender, ConsoleMessage msg)
    {
        GUIMessage GUImessage;
        switch (msg.type)
        {
            case ConsoleMessage.MessageTypes.Log:
                if(msg.source == "")
                    GUImessage = GUIMessage.SourcelessMessage(msg.content);
                else
                    GUImessage = GUIMessage.LogMessage(msg.source, msg.content);
                break;

            case ConsoleMessage.MessageTypes.Debug:
                throw new NotImplementedException();

            case ConsoleMessage.MessageTypes.Error:
                GUImessage = GUIMessage.ErrorMessage(msg.source, msg.content);
                break;

            default:
                GUImessage = GUIMessage.EmptyMessage();
                break;
        }

        outputMessages.Add(GUImessage);
        scroll.y += outputLinesHeight;
    }


    Vector2 scroll;
    private void OnGUI()
    {
        GUI.skin = consoleSkin;

        if(!showConsole) return;
        
        // Unity.Input cannot be used while a GUI control is focused
        if(showConsole && Event.current.isKey) 
        {
            if(Event.current.type == EventType.KeyDown)
            {
                if(Event.current.keyCode == KeyCode.Return) ValidateInput();
                // if(Event.current.keyCode == KeyCode.Tab) Autocomplete();
                if(Event.current.keyCode == KeyCode.Escape) GUI.FocusControl(null);
            }
        }
        
        float x = consolePosition.x;
        float y = consolePosition.y;
        float outputHeight = consoleSize.y - inputHeight;

        // the entire console box
        GUI.Box(new Rect(x, y, consoleSize.x, consoleSize.y), "");

        // viewport rect y coordinate
        // guaranties the output is rendered as close as possible to the input field
        float vpy = Mathf.Max(5f, outputHeight - outputMessages.Count * outputLinesHeight - 5f);

        // the viewport is the reference rect in which the scroll view will navigate
        Rect viewport = new Rect(0, 0, consoleSize.x - 20f, outputMessages.Count * outputLinesHeight);
        
        // the scroll view is what we see of the viewport
        // the scroll rect is the rect in the viewport through which the viewport content is seen
        // the scroll vector is the coordinate the scroll rect is positioned at in the viewport
        scroll = GUI.BeginScrollView(new Rect(x, y + vpy, consoleSize.x, outputHeight - 5f), scroll, viewport);
        // anything created between GUI.BeginScrollView and GUI.EndScroll View
        // will be the content of the viewport (not the scroll view !)
        for (int i = 0; i < outputMessages.Count; i++)
        {
            GUIMessage message = outputMessages[i];
            GUI.contentColor = message.colour;
            GUI.Label(new Rect(0f, outputLinesHeight * i, viewport.width - 10f, outputLinesHeight), message.text);
        }
        GUI.EndScrollView();

        GUI.backgroundColor = new Color(0f, 0f, 0f, 1f);

        // if no prediction available, show in grey the input followed by three dots
        GUI.contentColor = Color.grey;
        if(controller.predictions.Count <= 0)
            GUI.Label(new Rect(x, y + outputHeight, consoleSize.x, inputHeight), input + "..."); 
        
        // if predictions are available, show them each on a new line, with a user defined tint
        GUI.contentColor = predictionColour;
        for (int i = 0; i < controller.predictions.Count; i++)
            GUI.Label(new Rect(x, y + outputHeight + 20.0f * i, consoleSize.x, inputHeight), controller.predictions[i]);

        // finally, show the input itself, on top of the rest (last) and in white
        GUI.contentColor = Color.white;
        input = GUI.TextField(new Rect(x, y + outputHeight, consoleSize.x, inputHeight), input);
    }


    private class GUIMessage
    {
        public enum ErrorTypes { MissingArgument, InvalidArgument, InvalidCommand };
        public static Color LogColour;
        public static Color ErrorColour;

        public readonly string text;
        public readonly Color colour;
        
        private GUIMessage(string text, Color? colour = null)
        {
            this.text = text;
            this.colour = colour ?? Color.white;
        }

        public static GUIMessage EmptyMessage()
        {
            return new GUIMessage("");
        }
        public static GUIMessage SourcelessMessage(string content, Color? colour = null)
        {
            return new GUIMessage(content, colour ?? LogColour);
        }
        public static GUIMessage LogMessage(string source, string content, Color? colour = null)
        {
            return new GUIMessage($"[{source}] {content}", colour ?? LogColour);
        }
        public static GUIMessage ErrorMessage(string source, string content, Color? colour = null)
        {
            return new GUIMessage($"[{(source != "" ? $"{source} | " : "")}Error] {content}", colour ?? ErrorColour);
        }
    }
}
