using System;
using System.Collections.Generic;

public partial class ConsoleController
{
    public static ActionCommand<string> HELP;
    public static ActionCommand CLIST;
    public static ActionCommand CLEAR;
    public static ActionCommand<string> PRINT;
    public static ActionCommand TRANSITION;

    // DEFINE NEW COMMANDS HERE

    public void CommandsInit()
    {
        // Here are initialized the different commands by assigning
        // an instance of a daughter class of CommandBase (eg. ActionCommand)
        // to a predefined static field
        
        HELP = new ActionCommand<string>("help", "Provides information about a command", "help [command_name]",
        (c) => {
            int index = FindCommandIndex(c);
            if(index >= 0 && index < commands.Count)
            {
                var com = commands[index];
                PrintToConsole(ConsoleMessage.Log($"\'{com.format}\' | {com.description}", "HELP"));
            }
            else
            {
                PrintToConsole(ConsoleMessage.Error(ConsoleMessage.ErrorTypes.InvalidCommand, c, "", "HELP"));
            }
        });

        CLIST = new ActionCommand("clist", "Prints a list of all the available commands", "clist",
        () => {
            PrintToConsole(ConsoleMessage.Log("Available commands:", "CLIST"));
            foreach (var com in commands)
            {
                PrintToConsole(ConsoleMessage.Log($"   {com.name} \'{com.format}\' : {com.description}"));
            }
        });

        CLEAR = new ActionCommand("clear", "Clears the console", "clear", 
        () => {
            ClearConsole?.Invoke(this, EventArgs.Empty);
        });

        PRINT = new ActionCommand<string>("print", "Prints the provided text in the console", "print [text]",
        (text) => {
            PrintToConsole(ConsoleMessage.Log(text));
        });

        TRANSITION = new ActionCommand("transition", "Triggers the transition animation", "transition",
        () => {
            GameHandler._instance.Transition();
        });

        // INITIALIZE NEW COMMANDS HERE


        // Only commands added here will be shown and usable in the console.
        commands = new List<CommandBase>
        {
            HELP,
            CLIST,
            CLEAR,
            PRINT,
            TRANSITION,
            // ADD NEW COMMANDS HERE
        };
    }
}
