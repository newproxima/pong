using UnityEngine;
using UnityEngine.SceneManagement;

public class PopupMenu : MonoBehaviour
{
    public void RetartButtonPressed()
    {
        SceneManager.LoadScene(1);
    }

    public void MenuButtonPressed()
    {
        SceneManager.LoadScene(0);
    }

    public void ExitButtonPressed()
    {
        Application.Quit();
    }
}
