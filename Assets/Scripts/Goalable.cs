using UnityEngine;

[RequireComponent(typeof(SpriteRenderer))]
public class Goalable : MonoBehaviour
{
    public event System.Action<string> Hit ;

    SpriteRenderer _renderer;
    Color leftGoalColor;
    Color rightGoalColor;
    Color defaultColour;

    void Awake()
    {
        _renderer = GetComponent<SpriteRenderer>();
    }

    void Start()
    {
        leftGoalColor = GameHandler._instance.leftColour;
        rightGoalColor = GameHandler._instance.rightColour;
        defaultColour = GameHandler._instance.defaultColour;

        SetAsNoTarget();
    }

    public void SetAsLeftPlayerTarget()
    {
        _renderer.color = leftGoalColor;
    }

    public void SetAsRightPlayerTarget()
    {
        _renderer.color = rightGoalColor;
    }

    public void SetAsNoTarget()
    {
        _renderer.color = defaultColour;
    }

    void OnCollisionEnter2D(Collision2D other)
    {
        Hit?.Invoke(gameObject.name);
    }
}
